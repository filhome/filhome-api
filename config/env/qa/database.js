const parse = require('pg-connection-string').parse;
const connectionString = process.env.DATABASE_URL || 'postgresql://filhome:rcGFCsEQq1ynFbeA@filhome-do-user-9896548-0.b.db.ondigitalocean.com:25060/filhome?sslmode=require'
const config = parse(connectionString);
console.log(config)
module.exports = () => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: config.host,
        port: config.port,
        database: config.database,
        username: config.user,
        password: config.password,
        ssl: {
          rejectUnauthorized: false,
        },
      },
      options: {},
    },
  },
});