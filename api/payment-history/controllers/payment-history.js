"use strict";
const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async createEx(ctx) {
    const { loan, amount } = ctx.request.body;
    const record = await strapi.services.loans.findOne({ id: loan });
    let entity;
    console.log(record);

    if (record) {
      entity = await strapi.controllers["payment-history"].create(ctx);

      const { paid_amount, total_amount } = record;
      const newAmount = paid_amount + amount;
      if (newAmount > total_amount) {
        return new Error("Over amount");
      }
      await strapi.services.loans.update(
        { id: loan },
        { paid_amount: newAmount }
      );
      return entity;
    }

    return sanitizeEntity(entity, { model: strapi.models["payment-history"] });
  },
};
