'use strict';
const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async updateEx(ctx) {
    const { id } = ctx.params;
    const record = await strapi.services.notification.findOne({ id });
    let entity;

    if (!record) {
      entity = await strapi.controllers.notification.create(ctx)
      return entity
    }

    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.notification.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.notification.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.notification });
  },
};
